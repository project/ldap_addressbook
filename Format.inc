<?php //Dear emacs, please make this buffer -*- php -*-


/**
 * @file
 * This file contains a set of constructions that help us formatting the
 * output of LDAP searches and views.
 *
 * Copyright (C) 2006 Andre dos Anjos
 *
 * This file is part of the ldap_addresbook module for Drupal.
 *
 * The ldap_addressbook module is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 **/

/**
 * Returns the name of a temporary file which contains the image given as
 * parameter rescaled to fit the given maximum size parameter. The rescaling
 * executed by this function will have optimal resampling.
 *
 * @param $original_imagefilename The input jpeg image file
 * @param $size_hint The maximum size hint for this image
 * @param $tmp_path The temporary directory where to put the resulting file
 * @param $prefix A string prefix, so I can generate a filename that is easy
 * to identify
 **/
function _lab_rescale_file($original_imagefilename, $size_hint,
                           $tmp_path, $prefix) {
  $value = getimagesize($original_imagefilename);
  if (!$value) return FALSE;
  list($width_orig, $height_orig, $type_orig, $attr_orig) = $value;

  $width = $width_orig;
  $height = $height_orig;
  if ($width > $height) {
    if ($width > 0) $height = ((float)$size_hint/$width) * $height_orig;
    $width = $size_hint;
  }
  else {
    if ($height > 0) $width = ((float)$size_hint/$height) * $width_orig;
    $height = $size_hint;
  }
  //trigger_error('Resizing image to (w x h) = ('.$width.' x '.$height.')');

  // Resample
  $resampled_image = imagecreatetruecolor($width, $height);
  $image = imagecreatefromjpeg($original_imagefilename);
  imagecopyresampled($resampled_image, $image, 0, 0, 0, 0, $width, $height,
                     $width_orig, $height_orig);

  // Save again
  $resampled_imagefilename = tempnam($tmp_path, $prefix);
  imagejpeg($resampled_image, $resampled_imagefilename, 100);

  // Destroy the existing images
  imagedestroy($image);
  imagedestroy($resampled_image);

  // Return happily
  chmod($resampled_imagefilename, 0644);
  return $resampled_imagefilename;
}

/**
 * Returns the name of a temporary file which contains the image given as
 * parameter rescaled to fit the given maximum size parameter. The rescaling
 * executed by this function will have optimal resampling.
 *
 * @param $jpeg The input jpeg image
 * @param $size_hint The maximum size hint for this image
 * @param $tmp_path The temporary directory where to put the resulting file
 * @param $prefix A string prefix, so I can generate a filename that is easy
 * to identify
 **/
function _lab_rescale($jpeg, $size_hint, $tmp_path, $prefix) {
  //save the jpeg initially
  $original_imagefilename = tempnam($tmp_path, $prefix);
  $original_imagefile = fopen($original_imagefilename, 'wb');
  fwrite($original_imagefile, $jpeg);
  fclose($original_imagefile);
  $resampled_imagefilename = 
    _lab_rescale_file($original_imagefilename, $size_hint,
                      $tmp_path, $prefix);
  // Remove the original file
  unlink($original_imagefilename);
  return $resampled_imagefilename; //this may be false
}

/**
 * This function translates the names and meanings of most of the LDAP fields
 * of interest. If you have any strings wishing translation, just place them
 * here.
 *
 * @param k The name of the string needing translation
 * @return The human-readable format of the string, if available, or itself.
 **/
function _lat($k) {
  $dict = array('cn' => t('Name'),
                'givenname' => t('First name'),
                'sn' => t('Last name'),
                'jpegphoto' => t('Photo'),
                'mail' => t('E-mail'),
                'homepostaladdress' => t('Home address'),
                'postaladdress' => t('Work address'),
                'postalcode' => t('Work postal code'),
                'postalcode' => t('Work zip'),
                'initials' => t('Initials'),
                'title' => t('Title'),
                'telephonenumber' => t('Telephone'),
                'homephone' => t('Private phone number'),
                'mobile' => t('Mobile'),
                'o' => t('Organization'),
                'labeleduri' => t('Web address'));
  $lk = strtolower($k);
  if (array_key_exists($lk, $dict)) {
    return $dict[$lk];
  }
  return $k;
}

/**
 * This function can output a Drupal themed table that contains 1 addressbook
 * contact in compact form. This is the reasoning behind this view:
 * # The user photo, if there is any, is reduced
 * # The phone numbers are all cluttered together
 * # The mail and URL are grouped together
 * # We use graphical icons to show the view/edit/delete urls, since they are
 * more compact than text.
 *
 * @param $info The entry from the LDAP server
 * @param $summary The summary fields I'm supposed to work with
 * @param $path The path to the module
 * @param $tmp_path The temporary directory where I should be able to create
 * temporary files.
 * @param $edit TRUE if the user has edit permissions (this will make me
 * generate links for the edit and delete panes.
 * @param $privacy The privacy keyword to fill in to the links (either 
 * 'global', for global addressbook entries or 'private').
 *
 * @param $info An LDAP entry for which you want to create a compact view.
 */
function _lab_compact_view($info, $summary, $path, $tmp_path, $edit, $privacy) {
  $cn = $info["cn"][0];
  $row = array();

  $imgdir = "modules/$path/image";

  
  $images = l(theme_image($imgdir.'/view.png', t('view'), t('view')),
              $path."/node/$privacy/$cn/view", array(), NULL, NULL, FALSE, TRUE);
  if ($edit) {
    $images .= '<br />'.
      l(theme_image($imgdir.'/edit.png', t('edit'), t('edit')), 
        $path."/node/$privacy/$cn/edit", array(), NULL, NULL, FALSE, TRUE).
      '<br />'.
      l(theme_image($imgdir.'/delete.png', t('delete'), t('delete')), 
        $path."/node/$privacy/$cn/delete", array(), NULL, NULL, FALSE, TRUE);
  }
  $row[] = $images;

  //jpeg photo comes first
  if (in_array('jpegphoto', $summary)) {
    if(array_key_exists('jpegphoto', $info)) {
      $imagefilename = _lab_rescale($info['jpegphoto'][0], 70,
                                    $tmp_path, $path);
      if (!$imagefilename) {
        drupal_set_message(t("JPEG image for contact '$cn' seems corrupted. Fix it whenever possible!"));
        $row[] = l(t("[Corrupted image]"),
                   $path."/node/$privacy/$cn/view", array(), NULL, NULL, FALSE, TRUE);
      }
      else {
        $row[] = l(theme_image($imagefilename, '['.$info['cn'][0].'\'s photo]',
                               '['.$info['cn'][0].'\'s photo]'),
                   $path."/node/$privacy/$cn/view", array(), NULL, NULL, FALSE, TRUE);
      }
    }
    else $row[] = l(t("[No photo]"),
                    $path."/node/$privacy/$cn/view", array(), NULL, NULL, FALSE, TRUE);
;  
  }

  //CN gets the e-mail link and the URL under it
  $newcolumn = "<b>$cn</b><br /><br />";
  if (array_key_exists('mail', $info)) {
    $mail = $info['mail'][0];
    $newcolumn .= '<a href="mailto:'.$mail.'">'.
      theme_image($imgdir.'/mail.png', t("e-mail to $cn"), t("e-mail to $cn"), 
                  array("style" => " vertical-align:middle")).$mail.'</a>';
  }
  if (array_key_exists('labeleduri', $info)) {
    $url = $info['labeleduri'][0];
    $newcolumn .= '<br /><a href="'.$url.'">'.
      theme_image($imgdir.'/home.png', t("visit $cn's homepage"), 
                  t("visit $cn's homepage"), 
                  array("style" => " vertical-align:middle")).$url.'</a>';
  }
  $row[] = $newcolumn;

  //Phone numbers
  $newcolumn = '';
  if (array_key_exists('telephonenumber', $info)) 
    $newcolumn .= '<b>'.t('Main').'</b>:'.$info['telephonenumber'][0];
  if (array_key_exists('homephone', $info)) 
    $newcolumn .= '<br />'.
      theme_image($imgdir.'/home.png', t("visit $cn's homepage"), 
                  t("visit $cn's homepage"), 
                  array("style" => " vertical-align:middle")).
      $info['homephone'][0];
  if (array_key_exists('mobile', $info)) 
    $newcolumn .= '<br /><b>'._lat('mobile').':</b>'.$info['mobile'][0];
  $row[] = $newcolumn;
  
  $exclude = array('cn', 'jpegphoto', 'mail', 'labeleduri',
                   'telephonenumber', 'homephone', 'mobile'); 
  foreach ($summary as $field) {
    if (in_array($field, $exclude)) continue;
    $value = $info[$field][0]; 
    if ($field == 'mail')   
      $row[] = "<a href=\"mailto:$value\">$value</a>";
    else if ($field == 'labeleduri')
      $row[] = "<a href=\"$value\">$value</a>";
    else
      $row[] = $value;
  }
  return $row;
}

/**
 * This function can output a Drupal themed table that contains many
 * addressbook contacts in compact format.
 *
 * @param $info The entries from the LDAP server
 * @param $fields The fields I'm supposed to work with
 * @param $path The path to the module
 * @param $tmp_path The temporary directory where I should be able to create
 * temporary files.
 * @param $edit TRUE if the user has edit permissions (this will make me
 * generate links for the edit and delete panes.
 * @param $privacy The privacy keyword to fill in to the links (either 
 * 'global', for global addressbook entries or 'private').
 *
 * @param $info An LDAP entry for which you want to create a compact view.
 */
function _lab_multi_view($info, $summary, $path, $tmp_path, $edit, $privacy) {
  $table_head = array();
  $table_row = array();
  $table_head[] = '';
  //jpeg photo comes first
  if (in_array('jpegphoto', $summary)) $table_head[] = _lat('jpegphoto');

  $table_head[] = _lat('cn');

  if (in_array('telephonenumber', $summary) || 
      in_array('homephone', $summary) ||
      in_array('mobile', $summary));
  $table_head[] = _lat('telephonenumber');

  $exclude = array('cn', 'jpegphoto', 'mail', 'labeleduri',
                   'telephonenumber', 'homephone', 'mobile'); 

  foreach ($summary as $field) {
    if (in_array($field, $exclude)) continue;
    $table_head[] = l(_lat($field), $path."/search/$privacy/$what/$field");
  }
  for ($i=0; $i<count($info); $i++) 
    $table_row[] = _lab_compact_view($info[$i], $summary, $path, $tmp_path,
                                     $edit, $privacy);
  $table_attrs = array("width" => "100%");
  return theme_table($table_head, $table_row, $table_attrs);
}

/**
 * This function can output a Drupal themed table that contains 1 addressbook
 * contact in extended form.
 *
 * @param $info The entry from the LDAP server
 * @param $fields The fields I'm supposed to work with
 * @param $path The path to the module
 * @param $tmp_path The temporary directory where I should be able to create
 * temporary files.
 *
 * @param $info An LDAP entry for which you want to create a compact view.
 */
function _lab_view($info, $fields, $path, $tmp_path) {
  $table_head = array();
  $table_row = array();
  $cn = $info['cn'][0];
  if(array_key_exists('jpegphoto', $info)) {
    $name_entry = array('data' => '<h2>'.$cn.'</h2>');
    $imagefilename = 
      _lab_rescale($info['jpegphoto'][0], 100, $tmp_path, $path);
    if (!$imagefilename) {
      drupal_set_message(t("JPEG image for contact '$cn' seems corrupted. Fix it whenever possible!"));
      $image_entry = '[Corrupted image]';
    }
    else {
      $image_entry = theme_image($imagefilename, '['.$cn.'\'s photo]',
                                 '['.$cn.'\'s photo]');
    }
    $table_row[0] = array($image_entry, $name_entry);
  }
  else { 
    $name_entry = array('data' => '<h2>'.$cn.'</h2>',
                        'colspan' => 2);
    $table_row[0] = array($name_entry);
  }
  
  $special = array('jpegphoto', 'cn');
  $i = 1;
  foreach ($fields as $field) {
    $lfield = strtolower($field);
    if (in_array($field, $special)) continue;
    $value = '';
    if (array_key_exists($field, $info))
      $value = $info[$field][0];
    if (array_key_exists($lfield, $info))
      $value = $info[$lfield][0];
    if ($lfield == 'mail' && !empty($value))
      $value = "<a href=\"mailto:$value\">$value</a>";
    else if ($lfield == 'labeleduri' && !empty($value))
      $value = "<a href=\"$value\">$value</a>";
    if (!empty($value)) {
      $value_entry = array('data' => $value);
      $table_row[$i] = array('<b>'._lat($field).':</b>', $value_entry);
      $i++;
    }
  }
  //$table_attrs = array("width" => "100%");
  return theme_table($table_head, $table_row, $table_attrs);
}

?>
