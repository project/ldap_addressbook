LDAP Address book for Drupal
----------------------------

1. INTRODUCTION

This module will allow you to maintain an address book with LDAP as
backend through Drupal. It is, in a sense, experimental work, so, feedback is
most appreciated.

2. INSTALLATION

Simply unpack the tarball to your modules/ directory and enable it in
the administration panel. Fill in sensible values at the options available
at the admin/settings panel. Most of them have descriptions.

For the time being, this interface will only allow you to add contacts into
LDAP folders (directories), later on, we plan to add some administration
capabilities into the interface as well. So, if you decide to give your
users some private addressbooks, just be sure to create them manually on 
your server. The module will detect problems in that area automatically
and will handle them accordingly.

3. WARNING

Be aware that the "writer" DN and password are saved in the database,
and that they are sent in clear text between your browser and the
webserver (unless, of course, you are using https). After the password is
stored in the drupal DB, anyone with access to that database will have
access to your password, but it will no longer circulate via the web
connection.

4. TODO

- There is a todo list on the top of the file ldap_addressbook.module, which
can be used for ideas for contributions. I should transplant it here in the
near future. Stay tunned.

